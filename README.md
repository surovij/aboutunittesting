## To run tests with coverlet use: 
dotnet test --collect:"XPlat Code Coverage"
## To install Stryker mootator use:
dotnet tool install -g dotnet-stryker
## To run stryker tests use: 
dotnet stryker -tr DotnetTest