﻿namespace AboutUnitTesting.MockTesting
{
    public class PersonUpdate
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}