﻿using System.Collections.Generic;

namespace AboutUnitTesting.MockTesting
{
    public interface IPersonRepository
    {
        IReadOnlyList<PersonEntity> GetPersons();
        PersonEntity GetPerson(int id);
        string GetPersonDetails(int id);
        string Update(PersonEntity person);
    }
}