﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace AboutUnitTesting.MockTesting
{
    public class PersonService
    {
        private readonly IUserAccessService _userAccessService;
        private readonly IPersonRepository _personRepository;
        private readonly ILogger _logger;

        public PersonService(
            IUserAccessService userAccessService,
            IPersonRepository personRepository,
            ILogger<PersonService> logger)
        {
            _userAccessService = userAccessService;
            _personRepository = personRepository;
            _logger = logger;
        }

        public IReadOnlyList<Person> GetPersons()
        {
            var personDbItems = _personRepository.GetPersons();
            
            return personDbItems
                .Select(ToPerson)
                .ToList();
        }

        public Person GetPerson(int id)
        {
            var personDbItem = _personRepository.GetPerson(id);
            var person = ToPerson(personDbItem);
            if (_userAccessService.IsAdmin() || _userAccessService.IsOwner())
            {
                var details = _personRepository.GetPersonDetails(id);
                person.Details = details;
            }

            _logger.LogInformation("My Log Message");
            return person;
        }

        public void Update(PersonUpdate personUpdate)
        {
            var person = _personRepository.GetPerson(personUpdate.Id);

            person.FirstName = personUpdate.FirstName;
            person.LastName = personUpdate.LastName;

            _personRepository.Update(person);
        }
        
        public void Update1(PersonUpdate personUpdate)
        {
          
            var person = _personRepository.GetPerson(personUpdate.Id);
            
            person.FirstName = personUpdate.FirstName;
            person.LastName = personUpdate.LastName;
            
            _personRepository.Update(person);
        }

        private static Person ToPerson(PersonEntity entity)
        {
            return new Person
            {
                Id = entity.Id,
                Name = $"{entity.FirstName} {entity.LastName}"
            };
        }
    }
}