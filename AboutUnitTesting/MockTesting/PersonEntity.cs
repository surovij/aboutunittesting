﻿namespace AboutUnitTesting.MockTesting
{
    public class PersonEntity
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        public string Details { get; set; }
    }
}