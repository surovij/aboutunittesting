﻿namespace AboutUnitTesting.MockTesting
{
    public interface IUserAccessService
    {
        bool IsAdmin();
        bool IsOwner();
    }
}