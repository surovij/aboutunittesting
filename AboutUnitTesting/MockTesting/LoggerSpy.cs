﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace AboutUnitTesting.MockTesting
{
    public class LoggerSpy<T> : ILogger<T>
    {
        public Exception CurrentException { get; private set; }
        public List<KeyValuePair<string, object>> State { get; } = new List<KeyValuePair<string, object>>();

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
            Func<TState, Exception, string> formatter)
        {
            CurrentException = exception;
            var logs = state as IReadOnlyList<KeyValuePair<string, object>> ?? new List<KeyValuePair<string, object>>();
            State.AddRange(logs);
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }
    }
}