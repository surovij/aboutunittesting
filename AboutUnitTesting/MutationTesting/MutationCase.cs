﻿using System;

namespace AboutUnitTesting.MutationTesting
{
    public class MutationCase
    {
        public void Run(int i)
        {
            if (i > 0 || i == -1)
            {
                Console.WriteLine(i);
            }
        }

        public void Run(int a, int b)
        {
            if (a > b || b < 21)
            {
                Console.WriteLine("true");
            }
            else
            {
                Console.WriteLine("false");
            }
        }
    }
}