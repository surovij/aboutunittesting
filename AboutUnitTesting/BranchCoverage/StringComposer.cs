﻿namespace AboutUnitTesting.BranchCoverage
{
    public class StringComposer
    {
        public string? Compose(string? s)
        {
            if (s != null)
            {
                s += "aaaaa";
            }
            var result = s?.Substring(1,s.Length -1);
            return result;
        }
    }
}