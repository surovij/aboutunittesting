﻿using System;

namespace AboutUnitTesting.Callback
{
    public class CallbackSample
    {
        private readonly ICallbackSampleService _service;

        public CallbackSample(ICallbackSampleService service)
        {
            _service = service;
        }

        public void CallService()
        {
            var token = Guid.NewGuid().ToString();
            _service.Call(token);
        }
    }
}