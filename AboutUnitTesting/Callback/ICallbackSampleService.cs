﻿namespace AboutUnitTesting.Callback
{
    public interface ICallbackSampleService
    {
        void Call(string token);
    }
}