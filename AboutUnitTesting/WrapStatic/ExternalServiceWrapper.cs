﻿namespace AboutUnitTesting.WrapStatic
{
    public class ExternalServiceWrapper
    {
        public void CallExternalService(int id)
        {
            var url = $"baseUrl/{id}";

            Call(url);
           
        }

        protected virtual void Call(string url)
        {
            ExternalService.Call(url);
        }
    }
}