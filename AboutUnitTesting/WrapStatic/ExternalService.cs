﻿using System;

namespace AboutUnitTesting.WrapStatic
{
    public static class ExternalService
    {
        public static void Call(string url)
        {
            Console.Write(url);
        }
    }
}