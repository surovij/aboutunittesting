﻿using AboutUnitTesting.BranchCoverage;
using FluentAssertions;
using Xunit;

namespace AboutUnitTestingTests.BranchCoverage
{
    public class StringComposerTests
    {
        private readonly StringComposer _stringComposer;

        public StringComposerTests()
        {
            _stringComposer = new StringComposer();
        }

        [Fact]
        public void Compose()
        {
            var result = _stringComposer.Compose("aaa");

            result!.Length.Should().Be(7);
        }

        [Fact]
        public void Compose1()
        {
            var result = _stringComposer.Compose(null);

            result.Should().BeNull();
        }
    }
}