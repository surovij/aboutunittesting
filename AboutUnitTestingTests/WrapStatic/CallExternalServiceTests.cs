﻿using AboutUnitTesting.WrapStatic;
using Xunit;

namespace AboutUnitTestingTests.WrapStatic
{
    public class CallExternalServiceTests
    {
        private readonly ExternalServiceWrapperSpy _externalServiceWrapperSpy;

        public CallExternalServiceTests()
        {
            _externalServiceWrapperSpy = new ExternalServiceWrapperSpy();
        }

        [Fact]
        public void Call()
        {
            var id = 1;

            _externalServiceWrapperSpy.CallExternalService(id);

            Assert.Equal($"baseUrl/{id}", _externalServiceWrapperSpy.Url);
        }

        private class ExternalServiceWrapperSpy : ExternalServiceWrapper
        {
            public string Url;
            
            protected override void Call(string url)
            {
                Url = url;
            }
        }
    }
}