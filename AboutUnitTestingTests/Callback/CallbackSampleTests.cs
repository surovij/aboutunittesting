﻿using AboutUnitTesting.Callback;
using FluentAssertions;
using Moq;
using Xunit;

namespace AboutUnitTestingTests.Callback
{
    public class CallbackSampleTests
    {
        private readonly CallbackSample _callbackSample;
        private readonly Mock<ICallbackSampleService> _service;

        public CallbackSampleTests()
        {
            _service = new Mock<ICallbackSampleService>();
            _callbackSample = new CallbackSample(_service.Object);
        }

        [Fact]
        public void A()
        {
            string? token = null;
            _service
                .Setup(_ => _.Call(It.IsAny<string>()))
                .Callback<string>(s => token = s);
            
            _callbackSample.CallService();

            token!.Length.Should().Be(36);

            // _service
            //     .Verify(_ => _.Call(token), Times.Once);
        }
    }
}