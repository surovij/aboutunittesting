﻿using System.Collections.Generic;
using AboutUnitTesting.MockTesting;
using FluentAssertions;
using Moq;
using Xunit;

namespace AboutUnitTestingTests.MockTesting
{
    public class PersonServiceTests
    {
        private readonly Mock<IPersonRepository> _personRepository;
        private readonly PersonService _personService;
        private readonly Mock<IUserAccessService> _userAccessService;
        private readonly LoggerSpy<PersonService> _logger = new LoggerSpy<PersonService>();

        public PersonServiceTests()
        {
            _personRepository = new Mock<IPersonRepository>();
            _userAccessService = new Mock<IUserAccessService>();
            _personService = new PersonService(_userAccessService.Object, _personRepository.Object, _logger);
        }

        [Fact]
        public void GetPersons()
        {
            var personEntities = new List<PersonEntity>
            {
                new PersonEntity {Id = 1, FirstName = "John", LastName = "Doe"}
            };
            _personRepository
                .Setup(_ => _.GetPersons())
                .Returns(personEntities);

            var persons = _personService.GetPersons();

            persons.Should().NotBeNullOrEmpty();
            persons[0].Id.Should().Be(personEntities[0].Id);
            persons[0].Name.Should().Be($"{personEntities[0].FirstName} {personEntities[0].LastName}");

            _personRepository.Verify(_ => _.GetPersons(), Times.Once);
            _personRepository.VerifyNoOtherCalls();
        }

        [Fact]
        public void GetPerson()
        {
            var personEntity = new PersonEntity {Id = 1, FirstName = "John", LastName = "Doe"};

            _personRepository
                .Setup(_ => _.GetPerson(personEntity.Id))
                .Returns(personEntity);

            var persons = _personService.GetPerson(personEntity.Id);

            persons.Should().NotBeNull();
            persons.Id.Should().Be(personEntity.Id);
            persons.Name.Should().Be($"{personEntity.FirstName} {personEntity.LastName}");
            persons.Details.Should().BeNull();

            _logger.State.Should().NotBeNullOrEmpty();
            _logger.State[0].Value.Should().Be("My Log Message");

            _personRepository.Verify(_ => _.GetPerson(personEntity.Id), Times.Once);
            _personRepository.Verify(_ => _.GetPersonDetails(personEntity.Id), Times.Never);
            _personRepository.VerifyNoOtherCalls();
        }


        [Fact]
        public void GetPersonWithDetails_ForAdmin()
        {
            var personEntity = new PersonEntity {Id = 1, FirstName = "John", LastName = "Doe"};

            _personRepository
                .Setup(_ => _.GetPerson(personEntity.Id))
                .Returns(personEntity);

            var personDetails = "some person details";
            _personRepository
                .Setup(_ => _.GetPersonDetails(personEntity.Id))
                .Returns(personDetails);
            _userAccessService
                .Setup(_ => _.IsAdmin())
                .Returns(true);

            var persons = _personService.GetPerson(personEntity.Id);

            persons.Should().NotBeNull();
            persons.Id.Should().Be(personEntity.Id);
            persons.Name.Should().Be($"{personEntity.FirstName} {personEntity.LastName}");
            persons.Details.Should().Be(personDetails);

            _logger.State.Should().NotBeNullOrEmpty();
            _logger.State[0].Value.Should().Be("My Log Message");

            _personRepository.Verify(_ => _.GetPerson(personEntity.Id), Times.Once);
            _personRepository.Verify(_ => _.GetPersonDetails(personEntity.Id), Times.Once);
            _personRepository.VerifyNoOtherCalls();
            _userAccessService.Verify(_ => _.IsAdmin(), Times.Once);
            _userAccessService.VerifyNoOtherCalls();
        }

        [Fact]
        public void GetPersonWithDetails_ForOwner()
        {
            var personEntity = new PersonEntity {Id = 1, FirstName = "John", LastName = "Doe"};

            _personRepository
                .Setup(_ => _.GetPerson(personEntity.Id))
                .Returns(personEntity);

            var personDetails = "some person details";
            _personRepository
                .Setup(_ => _.GetPersonDetails(personEntity.Id))
                .Returns(personDetails);
            _userAccessService
                .Setup(_ => _.IsAdmin())
                .Returns(false);
            _userAccessService
                .Setup(_ => _.IsOwner())
                .Returns(true);

            var persons = _personService.GetPerson(personEntity.Id);

            persons.Should().NotBeNull();
            persons.Id.Should().Be(personEntity.Id);
            persons.Name.Should().Be($"{personEntity.FirstName} {personEntity.LastName}");
            persons.Details.Should().Be(personDetails);

            _logger.State.Should().NotBeNullOrEmpty();
            _logger.State[0].Value.Should().Be("My Log Message");

            _personRepository.Verify(_ => _.GetPerson(personEntity.Id), Times.Once);
            _personRepository.Verify(_ => _.GetPersonDetails(personEntity.Id), Times.Once);
            _personRepository.VerifyNoOtherCalls();
            _userAccessService.Verify(_ => _.IsAdmin(), Times.Once);
            _userAccessService.Verify(_ => _.IsOwner(), Times.Once);
           _userAccessService.VerifyNoOtherCalls();
        }

        [Fact]
        public void UpdatePerson()
        {
            var personUpdate = new PersonUpdate {Id = 1, FirstName = "Erik", LastName = "Erikson"};
            _personRepository
                .Setup(_ => _.GetPerson(personUpdate.Id))
                .Returns(new PersonEntity
                {
                    Id = personUpdate.Id,
                    FirstName = "John",
                    LastName = "Doe"
                });

            _personService.Update(personUpdate);

            _personRepository
                .Verify(_ => _.GetPerson(personUpdate.Id), Times.Once);
            _personRepository
                .Verify(_ => _.Update(It.Is<PersonEntity>(p =>
                    p.Id == personUpdate.Id &&
                    p.FirstName == personUpdate.FirstName &&
                    p.LastName == personUpdate.LastName)), Times.Once);
            _personRepository.VerifyNoOtherCalls();
        }
        
        [Fact]
        public void UpdatePerson1()
        {
            var personUpdate = new PersonUpdate {Id = 1, FirstName = "Erik", LastName = "Erikson"};
            _personRepository
                .Setup(_ => _.GetPerson(personUpdate.Id))
                .Returns(new PersonEntity
                {
                    Id = personUpdate.Id,
                    FirstName = "John",
                    LastName = "Doe"
                });

            _personService.Update1(personUpdate);

            _personRepository
                .Verify(_ => _.GetPerson(personUpdate.Id), Times.Once);
            _personRepository
                .Verify(_ => _.Update(It.Is<PersonEntity>(p =>
                    p.Id == personUpdate.Id &&
                    p.FirstName == personUpdate.FirstName &&
                    p.LastName == personUpdate.LastName)), Times.Once);
            _personRepository.VerifyNoOtherCalls();
        }
    }
}