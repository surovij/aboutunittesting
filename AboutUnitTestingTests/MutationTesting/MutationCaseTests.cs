﻿using AboutUnitTesting.MutationTesting;
using Xunit;

namespace AboutUnitTestingTests.MutationTesting
{
    public class MutationCaseTests
    {
        private readonly MutationCase _mutationCase;

        public MutationCaseTests()
        {
            _mutationCase = new MutationCase();
        }

        [Theory]
        [InlineData(1)]
        [InlineData(-1)]
        public void Run(int i)
        {
            _mutationCase.Run(i);
        }
        
        [Fact]
        public void Run1()
        {
            _mutationCase.Run(20,15);
        }
    }
}